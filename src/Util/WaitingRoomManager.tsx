/**
 * Contains modules which takes care of waiting room calls and callbacks
 *  TODO ADD SECURITY CHECKS TO PREVENT UNNECCESSARY DATA ACCESS
 * DB structure
 *      -waiting_room ( Collection Name)
 *          -ROOM_NAME
 *              -patien_in_room : boolean
 *              -allow_patient_in_room : boolean
 *              -consultation_name_url : string
 */


import { WaitingRoomDBInterface } from "../interfaces/waitingroom";
import WaitingRoom from "../Components/WaitingRoom";
import { Firebase } from "../firebase/firebase";
import app from "firebase/app";
 
var firebaseObject: Firebase | undefined = undefined;

const waitingRoomConfiguration = { room_name: "" };

var room_ref: app.database.Reference | undefined = undefined;

export const subScribeForRoomUpdates = (
  waitingRoomInterface: WaitingRoomDBInterface,
  fb: Firebase
) => {
  firebaseObject = fb;
  waitingRoomConfiguration.room_name = waitingRoomInterface.room_name;
  room_ref = firebaseObject.waitingRoomRef(waitingRoomInterface.room_name);
  room_ref.on("value", (snapshot) => {
    if (snapshot.val().allow_patient_in_room) {
      waitingRoomInterface.onDoctorsInvite &&
        waitingRoomInterface.onDoctorsInvite(snapshot.val().consultation_name_url); //TODO replace original consulation URL
    }else{
      waitingRoomInterface.onDoctorsInvite && 
        waitingRoomInterface.onDoctorsInvite("")
    }
  });
  notifyEnteredWaitingRoom();
};

export const unSubscribeFromRoom = () => {
  notifyLeavingWaitingRoom();
  room_ref?.off();
};

/**
 *  Called to inform the server that person have entered the waiting room
 */
const notifyEnteredWaitingRoom = () => {
  firebaseObject
    ?.waitingRoomRef(waitingRoomConfiguration.room_name)
    .update({ patient_in_room: true });
};

/**
 *  called to inform the patient is leaving the waiting room
 */

const notifyLeavingWaitingRoom = () => {
  firebaseObject
    ?.waitingRoomRef(waitingRoomConfiguration.room_name)
    .update({ patient_in_room: false });
};
