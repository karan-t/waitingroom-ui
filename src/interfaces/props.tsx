/**
 * enterWaitingRoom_cb callback function which will be called
 * when user presses on continue with current browser in a suboptimal browser
 */
export interface WarningProps{
    isBrowserSupported:boolean,
    enterWaitingRoom_cb?:Function,
    googleChromeOpenLink?:string,
    installGoogleChromeLink?:string

}
export interface WaitingRoomProps{
    room_name:string
}