export interface WaitingRoomDBInterface{
    room_name:string,
    onDoctorsInvite?:Function // Callback Function triggerd when doctor invite to Consulation Room
}