import {WarningProps} from './props'

export interface AppState{
    browserSupportConfig?:WarningProps, 
    allreadyWarned:boolean,
    room_name:string
}