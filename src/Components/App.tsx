/**
 * Expecting a url as https://waitingroom.server.com/sample_room
 * This will enter the sample room waiting room if available
 */
import React from 'react';
import logo from './logo.svg';
import '../CSS/App.css';
import verifyBrowserSupport from '../Util/browserVerifier'
import {AppState} from '../interfaces/states'
import PageLoader from './PageLoader'
import WaitingRoom from './WaitingRoom'
import {WarningProps} from '../interfaces/props'
import Warning from './Warning'


import { createBrowserHistory } from 'history';

class App extends React.Component<any>{
  constructor(props:any){
    super(props)
  }
  
  state : AppState = {allreadyWarned:false,room_name:"sample_room"}

  

  componentDidMount=()=>{
    var browserSupport : WarningProps = verifyBrowserSupport()
    console.log()
    browserSupport.enterWaitingRoom_cb = this.enterWaitingRoom

    this.setState({browserSupportConfig:browserSupport,room_name:this.getwaitingRoomURl()}) 
  }
  
  getwaitingRoomURl:()=>string = ()=>{
    var url_array =  window.location.href.split('/')
    return url_array[url_array.length-1]
  }
  enterWaitingRoom = ()=>{
    this.setState({allreadyWarned:true}) 
  }
  
  render=()=>{
    if(this.state.browserSupportConfig){
      if(this.state.browserSupportConfig.isBrowserSupported || this.state.allreadyWarned){
        return(<WaitingRoom room_name={this.state.room_name}></WaitingRoom>)
      }else{
        return(<Warning {...this.state.browserSupportConfig}></Warning>)
      }
    }else{
      return(<PageLoader></PageLoader>) // This condition will never run TODO: Make it run
    }
  }
}

export default App;
