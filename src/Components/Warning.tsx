import React from "react";
import { WarningProps } from "../interfaces/props";
import '../CSS/App.css'

const Warning = (props: WarningProps) => {
  return ( //Directly Copied from a non react page, TODO:// Make it reactive
    <div className="full-container">
      <div className="title-logo"></div>
      <div className="subtitle-expl">
        The teleconsulation works better in Google Chrome browser
      </div>
      <div className="chrome-available-container">
        <a id="chrome-logo-continaer" href={props.googleChromeOpenLink}></a>
        <div className="chrome-routing-text">
          <a id="go-to-chrome" href={props.googleChromeOpenLink}>
            Click here{" "}
          </a>
          <span> to open chrome if already available in your phone </span>
        </div>
      </div>
      <div className="continue-browser-container">
        <a
          id="continue-browser-logo"
          onClick={() => props.enterWaitingRoom_cb && props.enterWaitingRoom_cb()}
        ></a>
        <div className="continue-browser-text">
          <a id="continue-same-br" onClick={() => props.enterWaitingRoom_cb && props.enterWaitingRoom_cb()}>
            Click here
          </a>
          <span> to Continue with the current browser </span>
        </div>
      </div>
      <div className="download-chrome">
        <a id="google-chrome-playstore" href={props.installGoogleChromeLink}>
          Click here
        </a>
        <span> to install chrome </span>
      </div>
    </div>
  );
};
export default Warning;
