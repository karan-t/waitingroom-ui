import React, { Component } from 'react';
import {TextContainer} from './TextContainer';
import {VideoContainer} from './VideoContainer';
import {AreaContainer} from './AreaContainer';
import ASVisu from './AVisu';

export class WaitingRoomUI extends Component {

    render() {
      return(
        <>
        <div className = "text-vid-container">
            <TextContainer />      
            <VideoContainer />
            </div>
            <AreaContainer />
          </>
      )
    }
  
  }






 