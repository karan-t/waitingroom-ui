import React, { useContext, useEffect, useState } from 'react'
import FirebaseContext from '../firebase/context'
import { Firebase } from '../firebase/firebase';
import {subScribeForRoomUpdates,unSubscribeFromRoom} from '../Util/WaitingRoomManager'
import { WaitingRoomProps } from '../interfaces/props';
import {WaitingRoomUI} from './WaitingRoomUI';


const WaitingRoom=(props:WaitingRoomProps)=>{
    const Firebase = useContext(FirebaseContext)
    
    const [consulatationURl,setConsultationUrl] = useState("");

    useEffect(()=>{
        subScribeForRoomUpdates({room_name:props.room_name,onDoctorsInvite:onDoctors_Invite},Firebase)
        
    },[])
    useEffect(()=>{
    return informLeavingRoom
},[])
    //Call it appropirately
    const informLeavingRoom = ()=>{
        unSubscribeFromRoom()
    }
    const onDoctors_Invite = (new_room_url:string)=>{
        setConsultationUrl(new_room_url)
    }
    window.addEventListener('beforeunload',()=>{
        informLeavingRoom()
    })
    return(
        <>
        <WaitingRoomUI />
        <div>
            {consulatationURl !="" && <a onClick={()=>{
                informLeavingRoom()
                window.location.replace(consulatationURl)
            }}>Click here to join room</a>}
        </div>
        </>
    )
}
export default WaitingRoom;