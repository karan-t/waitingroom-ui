import React from 'react';
import Webcam from "react-webcam";

export const VideoContainer =()=>{

      if(window.innerHeight > window.innerWidth){
      const prop = {audio:true,width: window.innerHeight * 0.3, height: window.innerWidth * 0.3};

      return <div className="mobile-vid-container">    <Webcam {...prop}/>    </div>
}

else {
      const prop = {audio:true,width: window.innerWidth * 0.3, height: window.innerHeight * 0.3};

      return <div className="video-container">    <Webcam {...prop}/>    </div>   
}

}

