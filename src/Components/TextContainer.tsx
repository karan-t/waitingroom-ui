import React, { Component } from 'react';
import TimerReact from './TimerReact.jsx';

export class TextContainer extends Component {

    render() {
      return(
            <div className="text-container">
                <p id = "sub_1">Your meeting starts in</p>
                <div className='timer-react'> <TimerReact /></div>
                <p id = "sub_2"> Doctor has been notified</p>
                <p id = "sub_3">Please wait ...</p>
                </div>
      )
    }
}
