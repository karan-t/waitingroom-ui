import React, {Component} from 'react'
import {render} from 'react-dom'
import { Card, CardWrapper } from 'react-swipeable-cards';
 
export default class AdCardFinal extends Component {
  render() {
    const wrapperStyle = {
      backgroundColor: "#FFFFFF"
    }
    
    const cardStyle = {
      backgroundColor: "#FFFFFF",
      width: "30vw",
      height: "30vh"
    }
    return(
      <CardWrapper style={wrapperStyle}>
        <Card style={cardStyle}>
          First Ad Here
        </Card>
        <Card style={cardStyle}>
          Second Ad Here
        </Card>
        <Card style={cardStyle}>
          Third Ad Here
        </Card>
      </CardWrapper>
    );
  }
}