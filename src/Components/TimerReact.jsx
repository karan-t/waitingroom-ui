import React, { Component } from "react";
import Countdown, { zeroPad } from 'react-countdown';


const Completionist = () => <span>redirect</span>;

// Renderer callback with condition
const renderer = ({minutes, seconds, completed }) => {
  if (completed) {
    // Render a complete state
    return <Completionist />;
  } else {
    // Render a countdown
    return (
      <span>
      {zeroPad(minutes)}:{zeroPad(seconds)}
      </span>
    );
  }
};

export default class TimerReact extends Component {
    render() 
    {
      return(
        <Countdown date={Date.now() + 10000} renderer={renderer} />
      );
    }
  }