import app from 'firebase/app'
import "firebase/database"
import "firebase/storage"

export const firebaseConfig = {
    apiKey: "AIzaSyCRewEu4YZkZrBmG21Qxmi2jr2w8JjwyzA",
    authDomain: "test-a13ed.firebaseapp.com",
    databaseURL: "https://test-a13ed.firebaseio.com",
    projectId: "test-a13ed",
    storageBucket: "test-a13ed.appspot.com",
    messagingSenderId: "471055843776",
    appId: "1:471055843776:web:d993f493410808cb2b0825",
    measurementId: "G-8TVBQRT4ZP"
}


export class Firebase{

	reDB: app.database.Database
	storage: app.storage.Storage
    constructor() {
		app.initializeApp(firebaseConfig)
		this.reDB = app.database()
		this.storage = app.storage()
	}

    waitingRoomRef = (room_name:string)=>{
        return this.reDB.ref(`waiting_room/${room_name}`)
    }
}